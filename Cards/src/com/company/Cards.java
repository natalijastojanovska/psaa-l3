package com.company;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class Cards {
    public static int Solution (int card){
        Queue<Integer> queue = new LinkedList<Integer>();
        Stack<Integer> stack = new Stack<Integer>();
        for(int i = 0; i < 51; i++) {
            queue.add(i);
        }
        int i = 0;
        while(queue.peek() != card){
            for(int j = 0; j < 7; j++){
                stack.push(queue.remove());
            }
            while(!stack.isEmpty()){
                queue.add(stack.pop());
                queue.add(queue.remove());
            }
            i++;
        }
        return i;
    }

    public static void main (String[] args){
        System.out.println("Pick a card: ");
        Scanner scan = new Scanner(System.in);
        int card = scan.nextInt();
        int shuffle = Solution(card);
        System.out.println(shuffle);
    }

}
