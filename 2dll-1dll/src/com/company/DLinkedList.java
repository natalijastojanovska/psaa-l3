package com.company;
import com.company.Solution;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

class Node <E> {
    protected E data;
    protected Node <E> next;
    protected Node <E> prev;

    public Node() {
        data = null;
        next = null;
        prev=null;
    }

    public Node(E data, Node <E> prev, Node <E> next) {
        this.data = data;
        this.prev=prev;
        this.next = next;
    }
}

class DLinkedList <E> {
    private Node<E> head, tail;

    public DLinkedList(){
        head = null;
        tail = null;
    }

    public void insertFirst(E e) {
        Node <E> first = new Node (e,null,head);

        if(head!=null)
            head.prev=first;
        if(tail==null)
            tail=first;
        head=first;
    }

    public void insertLast (E e) {
        if(head!=null) {
            Node <E> last=new Node(e,tail,null);
            tail.next=last;
            tail=last;
        }
        else
            this.insertFirst(e);
    }

    public void insertAfter (E e, Node <E> n) {
        if(n==tail) {
            insertLast(e);
            return;
        }
        Node <E> node = new Node (e,n,n.next);
        n.next.prev=node;
        n.next=node;
    }

    public void insertBefore (E e, Node <E> n) {
        if(n==head) {
            insertFirst(e);
            return;
        }
        Node <E> node = new Node (e,n.prev,n);
        n.prev.next=n;
        n.prev=n;
    }

    public void deleteFirst () {
        if(head!=null) {
            Node <E> node = head;
            if(head.next==null)
                head=tail=null;
            else {
                head=head.next;
                head.prev=null;
            }
        }
    }
    public void display() {
        Node <E> current = head;
        if(head == null) {
            System.out.println("List is empty");
            return;
        }
        while(current != null) {
            System.out.print(current.data + " ");
            current = current.next;
        }
    }


    public Node <E> getHead() {
        return head;
    }

    public Node <E> getTail() {
        return tail;
    }

    public static void main(String[] args) throws IOException {

    }
}
