package com.company;
import com.company.DLinkedList;

import java.util.Scanner;

public class Solution {
    public static void main (String[] args) {
        DLinkedList<Integer> d1 = new DLinkedList<Integer>();
        DLinkedList<Integer> d2 = new DLinkedList<Integer>();

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter number of elements in the first list: ");
        int el1 = scan.nextInt();
        System.out.println("Enter number of elements in the second list: ");
        int el2 = scan.nextInt();

        System.out.println("First list: ");
        for(int i = 0; i < el1; i++){
            d1.insertLast(scan.nextInt());
        }

        System.out.println("Second list: ");
        for(int i = 0; i < el2; i++){
            d2.insertLast(scan.nextInt());
        }

        DLinkedList<Integer> nova = newList(d1, d2);
        System.out.println("New list: ");
        nova.display();
    }

    public static DLinkedList<Integer> newList(DLinkedList<Integer> d1, DLinkedList<Integer> d2) {

        DLinkedList<Integer> nova = new DLinkedList<Integer>();
        Node<Integer> p1 = d1.getHead();
        Node<Integer> p2 = d2.getTail();

        while(p1 != null && p2 != null){
            if(p1.data + p1.next.data == p2.data + p2.prev.data){
                nova.insertLast(p1.data + p1.next.data);
            }
            else {
                nova.insertLast(0);
            }
            p1 = p1.next.next;
            p2 = p2.prev.prev;
        }
        while(p1 != null){
            nova.insertLast(p1.data);
            p1 = p1.next;
        }
        while (p2 != null){
            nova.insertLast(p2.data);
            p2 = p2.prev;
        }
        return nova;
    }

}